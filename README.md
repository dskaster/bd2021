# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* 5COP009 - Bancos de Dados (Universidade Estadual de Londrina)

### Platform and tools ###
* JavaEE 7 Web
* Tomcat 9
* Netbeans 12
* PostgreSQL 14

### Dependencies ###
* PostgreSQL driver
* JSTL
* GSON
* Bootstrap
* jQuery

### Database configuration ###

```sql
CREATE SCHEMA j2ee;
CREATE TABLE j2ee.user
(
  id serial NOT NULL,
  login character varying(20) NOT NULL,
  senha character(32) NOT NULL,
  nome character varying(40) NOT NULL,
  nascimento date NOT NULL,
  avatar varchar(100),
  CONSTRAINT pk_user PRIMARY KEY (id),
  CONSTRAINT uq_user_login UNIQUE (login)
);
```


### How do I get set up? ###

Check: docs/Installation mini howto-Ubuntu 20.04 LTS.txt